using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;


public class SaveManager : MonoBehaviour
{
    public GameObject continueButton;

    private void Start()
    {
        //Checks if exists a save file, to show the continue button on start and game over
        CheckSaveData();
    }


    //Method to save player's last stage
    public void SavePlayer()
    {
        SaveSystem.SavePlayer();
    }

    //Method to load player's last stage
    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        Statics.CurrentLevel = data.lastStage;

    }

    public void CheckSaveData()
    {
        //Checks if continue button is assigned 
        if(continueButton == null)
        {
            return;
        }
        else
        {
            //If there is a save file, continue button appears
            if (SaveSystem.LoadPlayer() == null)
            {
                continueButton.SetActive(false);
            }
            else
            {
                continueButton.SetActive(true);
            }
        }
        
    }
}
