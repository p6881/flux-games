using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dodge : MonoBehaviour
{
    public GameObject enemy;
   
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    //Trigger that moves the enemy away from player shots
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Shot")
        {
            Rigidbody2D rgdb = enemy.GetComponent<Rigidbody2D>();
            
            rgdb.MovePosition(enemy.transform.position * 2);
        }
    }
}
